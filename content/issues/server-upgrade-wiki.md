---
title: Server maintenance downtime for disk upgrade
date: 2019-04-19 12:30:00
resolved: true
resolvedWhen: 2019-04-19 12:37:00
# down, disrupted, notice
severity: disrupted
affected:
  - Wiki
  - Build manager
section: issue
---

*Resolved* -
Server upgrade was successful

vps4.bramix.nl will be put in maintenance mode for a few minutes for upgrading the disk.
During this time the major downtime will be that the wiki will be down and the build server
won't be processing hooks.
