---
title: IPv6 certificate issue on postmarketos.org
date: 2018-11-22 00:00:00
resolved: true
resolvedWhen: 2018-11-22 00:05:00
# down, disrupted, notice
severity: disrupted
affected:
  - Website
section: issue
---

*Resolved* -
It turns out to be an caching issue with the SSL certificates in the backend, causing hosting 4 to have an older certificate than hosting 5.
Currently the IPv4 traffic is routed through an DigitalOcean floating IP which sends traffic to hosting 5 and redirecting to hosting 4 when there's an issue.
Since DigitalOcean doesn't have a floating IP feature for IPv6 and hosting 5 currently isn't in an IPv6 region those requests are allways served by hosting 4.
The certificate on hosting 4 was up to date but HAProxy wasn't reloaded when the certificates were refreshed causing it to serve pages on IPv6 with an old certificate.

*Investigating* -
Reports are coming in that for some users the certificate has expired and for some users everything works fine.
