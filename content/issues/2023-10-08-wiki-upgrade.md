---
title: Wiki upgrade
date: 2023-10-08 12:30:00
resolved: true
resolvedWhen: 2023-10-08 16:00:00
# down, disrupted, notice
severity: notice
affected:
  - Wiki
section: issue
---

*Announcement* -
Due to scheduled maintenance the wiki might not be available while we upgrade
both Mediawiki and the underlying operating system to a newer version.

*Resolved* -
Mediawiki has been successfully updated to the latest LTS version, v1.39.5.
