---
title: build.postmarketos.org migration
date: 2024-01-09 18:50:00
resolved: true
resolvedWhen: 2024-01-09 21:15:00
# down, disrupted, notice
severity: disrupted
affected:
  - Build manager
  - Binary repository
section: issue
---

*Announcement* -
Due to migration of the BPO service to a new server, build.postmarketos.org,
mirror.postmarketos.org, images.postmarketos.org and pkgs.postmarketos.org
won't be available intermittently.
