---
title: "MediaWiki upgrade to 1.43"
date: 2024-12-31 17:20:00
section: issue
resolved: true
resolvedWhen: 2024-12-31 18:00:00
severity: disrupted
affected:
  - Wiki
---

We're going to upgrade our Wiki from MediaWiki version 1.39 LTS to 1.43 LTS.

This is estimated to cause a downtime of approximately 30 minutes.
