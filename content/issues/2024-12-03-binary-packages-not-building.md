---
title: "binary packages not building"
date: 2024-12-03 07:50:00
resolved: true
resolvedWhen: 2024-12-03 07:58:00
# down, disrupted, notice
severity: disrupted
affected:
  - Build manager
section: issue
---

Our build manager https://build.postmarketos.org uses https://builds.sr.ht to
run build jobs, which currently seems to have problems (jobs aren't starting).
We have reported the problem to the sr.ht IRC.

See also: https://status.sr.ht/
