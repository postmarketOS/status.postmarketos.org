---
title: Migration to gitlab.postmarketos.org
date: 2024-10-05 22:00:00
resolved: true
resolvedWhen: 2024-10-06 21:20:00
# down, disrupted, notice
severity: disrupted
affected:
  - GitLab
section: issue
---

*(Find updates at the bottom)*

On Sunday, 2024-10-06 00:00 CEST, we are migrating from gitlab.com to our
self-hosted instance at gitlab.postmarketos.org. It is expected to take the
whole day (a test import took 18 hours).

* Projects (pmbootstrap, pmaports, etc.) will be read-only during the
  migration, meaning that you can't create issues, merge requests or comment on
  these.

* Accounts in gitlab.postmarketos.org will be approved *after* the migration is
  done.

* Read [postmarketos#77](https://gitlab.com/postmarketOS/postmarketos/-/issues/77)
  for more information on the migration plan, why we migrate to a self-hosted
  gitlab instance in the first place and why to that instead of Forgejo,
  SourceHut, etc.

**Update 18:15 CEST:**

* Transfer is still ongoing for pmaports.git, but has completed for all other
  repositories.

* Mirroring of git repositories to gitlab.com has been set up
  [here](https://gitlab.com/postmarketOS/mirror).

After the transfer of all repositories has completed, the next steps are as
follows.

* Set up permissions for Core Contributors, Trusted Contributors, Active
  Contributors (see [team](https://postmarketos.org/team/)).

* Unarchive all imported repositories at gitlab.postmarketos.org. (The
  repositories at gitlab.com will stay archived.)

* Approval of accounts, so https://gitlab.postmarketos.org can be used. It will
  probably take a few more hours until we reach this point, please be patient.

* Various clean up tasks (e.g. recreating epics as milestones, ensuring/fixing
  push hooks to various sites like postmarketos.org, adjust all repos where we
  have gitlab.com/postmarketos hardcoded)

**Update 23:00 CEST:**

* Repositories are transferred. The transfer ran into a timeout for 125
  merge requests in pmaports.git, those were not transferred. For all old
  merge requests, we just recommend looking at the archived ones at gitlab.com
  (we won't delete them). These also have the proper users associated to
  comments, which was not possible with the import.

* It seems that all issues were imported successfully.

* All imported merge requests have been closed (as they are now owned by the
  "Administrator" user who was used during import). **If you had an open merge
  request that is still relevant, please re-submit it at
  gitlab.postmarketos.org.** We realize that this is inconvenient, but it was
  not possible to do this differently as users from gitlab.com could not be
  mapped to users on gitlab.postmarketos.org (even if they already existed and
  were approved at time of transfer).

* Accounts are being approved now. If you just registered, give us some time to
  review new account requests. We should be able to do it within a day usually.

* If you run into any issues, please write in the `postmarketos-devel`
  [IRC/matrix channel](https://wiki.postmarketos.org/wiki/Matrix_and_IRC).

* Some of the mentioned clean up tasks still need to be done, this will be
  tracked in [postmarketos#77](https://gitlab.postmarketos.org/postmarketOS/postmarketos/-/issues/77)
  (now on gitlab.postmarketos.org).
