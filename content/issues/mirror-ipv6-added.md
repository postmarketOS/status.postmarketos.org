---
title: Mirror subdomain gets IPv6 connectivity
date: 2023-10-04 18:00:00
resolved: true
resolvedWhen: 2023-10-04 18:00:00
# down, disrupted, notice
severity: notice
affected:
  - Binary repository
section: issue
informational: true
---

Previously our DNS config had an 'A' entry for mirror.postmarketos.org. This
has now been converted to a 'CNAME' which as a side effect means that
mirror.postmarketos.org is now resolvable from IPv6. Everything should work the
same as over IPv4 (http(s), rsync), but if you experience any issues please let us
know!
