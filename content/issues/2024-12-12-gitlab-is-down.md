---
title: "gitlab is down"
date: 2024-12-12 06:22:00
section: issue
resolved: true
resolvedWhen: 2024-12-12 06:49:00
# down, disrupted, notice
severity: disrupted
affected:
  - GitLab
---

Unfortunately gitlab.postmarketos.org is currently down. We have notified
OSUOSL. It is the middle of the night for them currently, so it will probably
take a bit until it comes back online. We are working hard with them to
prevent such outages in the future.

UPDATE: an automatic update went wrong, it is up again.
