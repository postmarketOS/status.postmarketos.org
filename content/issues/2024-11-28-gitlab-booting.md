---
title: "HTTP 502: Waiting for GitLab to boot"
date: 2024-11-28 08:00:00
resolved: true
resolvedWhen: 2024-11-28 19:29:00
# down, disrupted, notice
severity: down
affected:
  - GitLab
section: issue
---

GitLab is stuck in "Waiting for GitLab to boot". OSUOSL maintains the
instance for us (see [blog post](https://postmarketos.org/blog/2024/10/14/gitlab-migration/)),
and we have contacted them. Unfortunately it is a public holiday in the US, so
it looks like this won't be resolved today.

UPDATE: OSUOSL resolved it and added additional monitoring, so this should not
go unnoticed for so long in the future, if it happens again.
