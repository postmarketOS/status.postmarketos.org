---
title: "GitLab reboot to increase RAM and CPU"
date: 2024-12-08 17:30:00
section: issue
resolved: true
resolvedWhen: 2024-12-08 17:53:00
# down, disrupted, notice
severity: disrupted
affected:
  - GitLab
---

OSUOSL is rebooting gitlab.postmarketos.org to increase RAM and CPU, this
should help with making it more reliable.
