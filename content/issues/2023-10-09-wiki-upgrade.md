---
title: Wiki upgrade
date: 2023-10-09 19:50:00
resolved: true
resolvedWhen: 2023-10-09 20:40:00
# down, disrupted, notice
severity: disrupted
affected:
  - Wiki
section: issue
---

*Announcement* -
Due to scheduled maintenance the wiki might not be available while we upgrade
the Alpine host to a newer version.

*Resolved* -
The host for Mediawiki has been upgraded to the latest versions, including php
and mariadb.
