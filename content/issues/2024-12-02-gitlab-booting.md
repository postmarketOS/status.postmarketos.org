---
title: "Our GitLab is down"
date: 2024-12-02 10:00:00
resolved: true
resolvedWhen: 2024-12-02 16:18:00
# down, disrupted, notice
severity: down
affected:
  - GitLab
section: issue
---

GitLab is stuck in "Waiting for GitLab to boot". OSUOSL maintains the
instance for us (see [blog post](https://postmarketos.org/blog/2024/10/14/gitlab-migration/)),
and we have contacted them.

More information:
https://gitlab.postmarketos.org/postmarketOS/postmarketos/-/issues/87
