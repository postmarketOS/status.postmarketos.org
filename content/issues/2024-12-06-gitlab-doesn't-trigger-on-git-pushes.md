---
title: "GitLab doesn't trigger on git pushes"
date: 2024-12-06 08:35:00
resolved: true
resolvedWhen: 2024-12-06 08:48:00
# down, disrupted, notice
severity: disrupted
affected:
  - GitLab
section: issue
---

GitLab doesn't trigger on git pushes currently, which means MRs do not get
updated after pushing to a git repository. We have contacted OSUOSL about it.
