---
title: sr.ht outage
date: 2024-01-10 8:50:00
resolved: true
resolvedWhen: 2024-01-16 18:00:00
# down, disrupted, notice
severity: disrupted
affected:
  - Build manager
section: issue
---

*Announcement* -
There is currently a network outage at sr.ht:
https://status.sr.ht/issues/2024-01-10-network-outage/

Therefore we currently cannot build new packages for the binary
repository, build new images or accept new pmbootstrap patches.

pmbootstrap can be used to build your own packages or images in the meantime:
https://wiki.postmarketos.org/wiki/Pmbootstrap
