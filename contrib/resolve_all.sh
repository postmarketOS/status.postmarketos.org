#!/bin/sh

date="$(date -u +'%Y-%m-%d %H:%M:00')"

for i in content/issues/*.md; do
	if ! grep -q '^resolved: false' "$i"; then
		continue
	fi
	sed -i "s/^resolved: false/resolved: true\nresolvedWhen: $date/" "$i"
	echo "$i: resolved at $date"
done
