#!/bin/sh -e

read -p 'Enter post title: ' title

short_title="$(echo "$title" | tr -s ' ' '-')"
today="$(date -u +'%Y-%m-%d')"
filename="content/issues/$today-$short_title.md"
cat << EOF > "$filename"
---
title: "$title"
date: $(date -u +'%Y-%m-%d %H:%M:00')
section: issue
resolved: false
# down, disrupted, notice
severity: disrupted
affected:
  - Binary repository
  - Build manager
  - GitLab
  - Matrix
  - Website
  - Wiki
---

TODO:
- adjust 'affected' above
- consider adjusting 'date' (UTC, has been set to current time)
- replace this text with a description of the problem

EOF

[ -n "$EDITOR" ] && "$EDITOR" "$filename"
